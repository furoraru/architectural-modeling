# Системная инженерия

## Архитектурное моделирование компьютерных систем

## Лекция 2

## Предмет моделирования. Сущностная, субстанциональная парадигмы

Пенской А.В., 2022

----

## Предмет моделирования

### Конкретные объекты

![](figures/a-particular-table-and-two-particular-chairs.png)

----

### Обощённые объекты

![](figures/general-types-and-particular-things.png)

----

### Отношения и взаимосвязи

![](figures/general-and-particular-relationships.png)

----

### Изменения и действия

<div class="row"><div class="col">

![](figures/transformation.jpg)

</div><div class="col">

![](figures/gun-trace.jpg)

</div></div>







---

## субстанциональная парадигма

---

## Субстанциональная парадигма

